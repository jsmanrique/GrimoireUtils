#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 J. Manrique Lopez de la Fuente
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# Authors:
#     J. Manrique Lopez <jsmanrique@bitergia.com>
#

from elasticsearch import Elasticsearch, helpers
from elasticsearch_dsl import Search

import logging

def establish_connection(es_host):
    """ Function to estabilish connection with a running Elasticsearch
        Given an elasticsearch host, it returns an Elasticsearch client
    """

    es = Elasticsearch([es_host])

    if not es.ping():
        raise ValueError('Connection refused')
    else:
        logging.info('Connection established with {}'.format(es_host))
        return es

def create_index(es, index_name, index_mapping):
    """ Function to create an Elasticsearch index.
        Given an ES client (es), an index name (index_name) and its mapping
        (index_mapping), it removes any index with same name and create a
        new one.
    """

    es.indices.delete(index_name, ignore=[400, 404])
    es.indices.create(index_name, body=index_mapping)

    logging.info(index_name + ' index created')

def create_index_pattern(es, index_name, time_field_name):
    """ Function to create a Kibana index pattern to visualize data
        Given an ES client (es), an index (index_name) and its time field name
        (time_field_name), it creates a Kibana index pattern to visualize
        the data.
    """

    es.index(index='.kibana', doc_type='index-pattern', id=index_name, body={'title': index_name, 'timeFieldName': time_field_name})

    logging.info('{} Kibana index pattern created'.format(index_name))

def last_item_date(es, index_name, time_field_name):
    """ Function to get the last item's date in an index
    """

    search = Search(using = es)
    request = search.index(index_name).sort('-{}'.format(time_field_name))[0]
    last_item_date = request.execute()[0][time_field_name]

    logging.info('Last item in {} on {}'.format(index_name, last_item_date))

    return last_item_date

def last_item_origin_date(es, origin_uri):
    """ Function to get the last item's date in the raw index for an specific
        origin
    """

    search = Search(using = es)
    request = search.index('raw').query('match', origin=origin_uri).sort('-timestamp')[0]
    last_item_date = request.execute()[0][time_field_name]

    logging.info('Last item from {} on {}'.format(origin_uri, last_item_date))

    return last_item_date
