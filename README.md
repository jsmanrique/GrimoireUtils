# Grimoire Utils

This repo contains a set of Python utilities to be used by other Python scripts
that would like using some parts of [GrimoireLab](http://grimoirelab.github.io)
for quick OSS projects analysis.
