#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 J. Manrique Lopez de la Fuente
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# Authors:
#     J. Manrique Lopez <jsmanrique@bitergia.com>
#

""" Some constansts, mainly mappings that would be used to format data
"""

GITHUB_GIT = {
    "mappings": {
        "item": {
            "properties": {
                "date": {
                    "type": "date",
                    "format" : "E MMM d HH:mm:ss yyyy Z",
                    "locale" : "US"
                },
                "commit_id": {"type": "keyword"},
                "contributor_name": {"type": "keyword"},
                "contributor_email_domain": {"type": "keyword"},
                "file": {"type": "keyword"},
                "lines_added": {"type": "integer"},
                "lines_removed": {"type": "integer"},
                "github_owner": {"type": "keyword"},
                "github_repository": {"type": "keyword"}
            }
        }
    }
}

GITHUB_ISSUES = {
    "mappings": {
        "item": {
            "properties": {
                "date": {
                    "type": "date",
                    "format": "yyyy-MM-dd'T'HH:mm:ss'Z'"
                },
                "contributor_name": {"type": "keyword"},
                "title": {"type": "string"},
                "state": {"type": "keyword"},
                "issue_type": {"type": "keyword"},
                "url": {"type": "keyword"},
                "comments": {"type": "integer"},
                "closed_at": {
                    "type": "date",
                    "format": "yyyy-MM-dd'T'HH:mm:ss'Z'"
                },
                "time_to_solve": {"type": "integer"},
                "assignee_name": {"type": "keyword"},
                "github_owner": {"type": "keyword"},
                "github_repository": {"type": "keyword"}
            }
        }
    }
}

RAW = {
    "mappings": {
        "item": {
            "properties": {
                "uuid": {"type": "keyword"},
                "updated_on": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss"
                },
                "backend_name":{"type":"keyword"},
                "tag":{"type":"keyword"},
                "category":{"type":"keyword"},
                "timestamp":{
                    "type":"date",
                    "format": "yyyy-MM-dd HH:mm:ss.SSSSSS"
                },
                "backend_version":{"type":"keyword"},
                "data":{"type":"object"},
                "origin":{"type":"keyword"},
                "perceval_version":{"type":"keyword"}
            }
        }
    }
}
